<?php

//Cackle DLE Module v2.01 Windows 1251
define('CACKLE_TIMER', 1);
define('CACKLE_TIMER', 500);

define('URL_PATH', $config['http_home_url'] . "user/");
include ('engine/modules/cackle/cackle_sync.php');



class Cackle{

    function Cackle($init=true,$channel){
        
        $this->channel = $channel;

        if ($init){
            //$this->cackle_auth();

            if( !CackleAPI::cackle_get_param('cackle_monitor') ) {
                $object = new stdClass();
                $object->post_id = 0;
                $object->time = 0;
                $object->mode = "by_channel";
                $object->status = "finish";
                $object->counter = 0;
                CackleAPI::cackle_set_param('cackle_monitor',$object);
            }

            if( !CackleAPI::cackle_get_param('cackle_monitor_short') ) {
                $object = new stdClass();
                $object->post_id = 0;
                $object->time = 0;
                $object->mode = "by_channel";
                $object->status = "finish";
                CackleAPI::cackle_set_param('cackle_monitor_short',$object);
            }
            //initialize modified triger object if not exist

            if(!CackleAPI::cackle_get_param('cackle_modified_trigger')){
                $modified_triger = new stdClass();
                CackleAPI::cackle_set_param('cackle_modified_trigger',$modified_triger);
            }

            if(!CackleAPI::cackle_get_param('cackle_posts_update')){
                $modified_triger = new stdClass();
                CackleAPI::cackle_set_param('cackle_posts_update',$modified_triger);
            }

            $sync = new CackleSync();

            $monitor = $this->check_monitor();

            if(is_object($monitor)){
                if(CackleAPI::cackle_get_param('cackle_sync')== 1){
                    self::channel_timer(time(),$monitor->post_id);
                    $sync->init($monitor->post_id,$monitor->mode);
                }

            }
            
            //////////////////////
           // $sync = new CackleSync();
           // if ($this->channel_timer(CACKLE_TIMER,$channel)){
           //     $sync->init(1,'short');
            //}
            
            
            
            //////////////////////////
            $this->cackle_display_comments();

        }
    }
    function channel_handler() {
        $apix = new CackleAPI();
        $post_fields = (object) unserialize(POSTS_FIELDS);
        if(CackleAPI::cackle_get_param('cackle_sync') == 1){
            
                $chans=range(1, 100);
                $now=time()*1000;
                $i=0;
                $manual_sync_trigger=CackleAPI::cackle_get_param('cackle_channel_modified_trigger');
                if($manual_sync_trigger!=1){
                    $modified = CackleAPI::cackle_get_param("cackle_channel_modified_first",$now);
                    if(CackleAPI::cackle_get_param("cackle_channel_modified")==false){
                        CackleAPI::cackle_set_param("cackle_channel_modified",$now);
                    }
                }
                else{
                    $modified = CackleAPI::cackle_get_param("cackle_channel_modified");
                }

                while(sizeof($chans)==100){
                    $resp = json_decode($apix->get_all_channels(100,$i,$modified),true);
                    $chans = isset($resp['chans'])?$resp['chans']:'undefined';
                    if(isset($chans[0])){
                        foreach ($chans as $chan) {
                            $post_id = intval($chan);
                            if($post_id==0) continue;
                            $dbh = $apix->conn();
                            $sth = $dbh->prepare("SELECT *
                            FROM  ".PREFIX.POSTS_TABLE." where $post_fields->id = ? ORDER BY $post_fields->id ASC
                            LIMIT 1");
                            $sth->execute(array($chan['channel']));
                            $post = $sth->fetchAll(PDO::FETCH_OBJ);
                            if(!isset($post[0])) continue;

                            $post = $post['0'];
                            $post_field_id = $post_fields->id;
                            $post_id = $post->$post_field_id;

                            //save max modified to channel modified when proccess
                            if ($chan['modify'] > CackleAPI::cackle_get_param("cackle_channel_modified", 0)) {
                                CackleAPI::cackle_set_param("cackle_channel_modified", $chan['modify']);
                            }

                            $posts_update = CackleAPI::cackle_get_param('cackle_posts_update');
                            $posts_update->$post_id = 'm';
                            CackleAPI::cackle_set_param('cackle_posts_update', $posts_update);
                        }

                    }
                    else{

                    }
                    $i++;
                }
                CackleAPI::cackle_set_param('cackle_channel_modified_trigger',1);



            
        }
    }
    function check_monitor(){
        /* Check cackle_monitor for synchronizing process
         * Return post_id which needed to sync or -1 if not
         */

        $object = CackleAPI::cackle_get_param('cackle_monitor');

        $apix = new CackleAPI();
        $dbh = $apix->conn();
        $post = (object) unserialize(POSTS_FIELDS);
        if($object->mode=='by_channel'){


            //if sync is called by pages, we need pause for 30 sec from the last sync
            if($object->time + 15 > time()){
                return -1;
            }
            if($object->status=='inprocess' && $object->time + 120 > time()){
                //do nothing because in progress
                return -1;
            }
            if($object->status=='next_page'){
                // do sync with the same post
                $ret_object = new stdClass();
                $ret_object->post_id=$object->post_id;
                $ret_object->mode="";
                return $ret_object;
            }
            if ($object->status=='finish' || $object->time + 120 < time()){
                if (!isset($object->counter) || $object->counter > 10000) $object->counter = 0;

                if ($object->counter % 2) {
                    $mode = 'sync';
                } else {
                    $mode = 'updates';
                }
                switch ($mode) {
                    case 'sync':
                        //continue sync process
                        $object->counter = $object->counter+1;
                        CackleAPI::cackle_set_param('cackle_monitor', $object);
                        break;
                    case 'updates':
                        //starting getting updates and return -1 to prevent Sync proccess
                        $this->channel_handler();
                        $object->counter = $object->counter+1;
                        CackleAPI::cackle_set_param('cackle_monitor', $object);
                        return -1;
                        break;
                }
                $posts_update = CackleAPI::cackle_get_param('cackle_posts_update');
                $posts_update_counter = 0;
                foreach ($posts_update as $channel => $property) {
                    $next_post_id = $channel;
                    unset($posts_update->$channel); //delete updated channel
                    CackleAPI::cackle_set_param('cackle_posts_update', $posts_update);
                    $posts_update_counter++;
                    break;
                }
                if ($posts_update_counter == 0) return -1;
                $ret_object = new stdClass();
                $ret_object->post_id = $next_post_id;
                $ret_object->mode = "";
                return $ret_object;
            }
        }
        elseif($object->mode == 'all_comments'){
            if($object->status=='inprocess' && $object->time + 120 > time()){
                //don't start if all comments sync in progress
                return -1;
            }
            else{
                //we can't handle all_comments sync from here because it handles ajax requests, so
                //we should start sync again from the max

                $sth = $dbh->prepare("SELECT MAX($post->id) as max, MIN($post->id) as min
                            FROM  ".PREFIX.POSTS_TABLE);
                $sth->execute();
                $min_max_post_id = $sth->fetchAll(PDO::FETCH_OBJ);

                $min_max_post_id = $min_max_post_id[0];
                $max_post_id = $min_max_post_id->max;

                $object->post_id = $max_post_id;
                $object->mode = 'by_channel';

                $object_s = CackleAPI::cackle_get_param('cackle_monitor_short');
                $object_s->post_id = $max_post_id;
                $object_s->mode = 'by_channel';

                CackleAPI::cackle_set_param('cackle_monitor',$object);
                CackleAPI::cackle_set_param('cackle_monitor_short',$object_s);

            }
        }
    }
    
    function time_is_over($cron_time){
        $cackle_api = new CackleAPI();
        $get_last_time = CackleAPI::cackle_get_param("last_time");
        $now=time();
        if ($get_last_time==""){
            $set_time = CackleAPI::cackle_set_param("last_time",$now);
            return time();
        }
        else{
            if($get_last_time + $cron_time > $now){
                return false;
            }
            if($get_last_time + $cron_time < $now){
                $set_time = CackleAPI::cackle_set_param("last_time",$now);
                return $cron_time;
            }
        }
    }

    static function channel_timer($cron_time, $id){
        $cackle_api = new CackleAPI();
       // if(CackleAPI::cackle_get_param('prepare') == 0){
          //  $cackle_api->cackle_db_prepare();
       // }


        if ( $dbh = $cackle_api->conn()){
            $sth = $dbh->prepare("SELECT * FROM ".PREFIX."_cackle_channel WHERE id = ? ORDER BY ID ASC
                            LIMIT 1");
            $sth->execute(array($id));
            $get_last_time = $sth->fetchAll(PDO::FETCH_OBJ);
            //close conn
            $dbh = null;
        }
        else{
            return null;
        }

        //print_r($get_last_time);die();
        //$get_last_time = CackleAPI::cackle_get_param("last_time_" . $schedule . "_" . $_SERVER['HTTP_HOST'],0);
        $now = time();
        if (count($get_last_time)==0) {
            $dbh = $cackle_api->conn();
            $b = $dbh->prepare( "INSERT INTO ".PREFIX."_cackle_channel (id, time) VALUES (:id,:now) ON DUPLICATE KEY UPDATE time = :now");
            $b->bindParam(":id", $id);
            $b->bindParam(":now", $now);
            $b->execute();
            //close conn
            $dbh = null;

            return $now;
        } else {
            $get_last_time = $get_last_time['0']->time;
            if ($get_last_time + $cron_time > $now) {
                return false;
            }
            if ($get_last_time + $cron_time < $now) {

                $dbh = $cackle_api->conn();
                $b = $dbh->prepare( "INSERT INTO ".PREFIX."_cackle_channel (id, time) VALUES (:id,:now) ON DUPLICATE KEY UPDATE time = :now");
                $b->bindParam(":id", $id);
                $b->bindParam(":now", $now);
                $b->execute();
                //close conn
                $dbh = null;

                return $cron_time;
            }
        }

    }

    function cackle_auth() {
        $cackle_api = new CackleAPI();
        $siteApiKey = CackleAPI::cackle_get_param('cackle_siteApiKey');
        $timestamp = time();
        if ($_SESSION['dle_user_id']) {
            $user_id = $_SESSION['dle_user_id'];
            $user_info = $cackle_api->db_connect("select * from ".PREFIX."_users where user_id = $user_id");
            $user_info = $user_info[0];
            if($user_info["foto"] == ""){
                $avatar_path=NULL;
            }
            else{
                if($cackle_api->startsWith($user_info["foto"], 'http')){
                    $avatar_path=$user_info["foto"];
                }
                else{
                    $avatar_path=AVATAR_PATH. $user_info["foto"];
                }
            }
            $user = array(
                'id' => $user_id,
                'name' => $user_info["name"],
                'email' => $user_info["email"],
                'avatar' => $avatar_path,
                'www' => URL_PATH .$user_info["name"]
            );
            //var_dump(URL_PATH .$user_info["name"]);
            $user_data = base64_encode(json_encode($user));
        } else {
            $user = '{}';
            $user_data = base64_encode($user);
        }
        $sign = md5($user_data . $siteApiKey . $timestamp);
        return "$user_data $sign $timestamp";
    }


     function cackle_comments( $comment) {
        
        ?><li  id="cackle-comment-<?php echo $comment['id']; ?>">
              <div id="cackle-comment-header-<?php echo $comment['id']; ?>" class="cackle-comment-header">
                  <cite id="cackle-cite-<?php echo $comment['id']; ?>">
                  <?php if($comment['autor']) : ?>
                      <a id="cackle-author-user-<?php echo $comment['id']; ?>" href="#" target="_blank" rel="nofollow"><?php echo $comment['autor']; ?></a>
                  <?php else : ?>
                      <span id="cackle-author-user-<?php echo $comment['id']; ?>"><?php echo $comment['name']; ?></span>
                  <?php endif; ?>
                  </cite>
              </div>
              <div id="cackle-comment-body-<?php echo $comment['id']; ?>" class="cackle-comment-body">
                  <div id="cackle-comment-message-<?php echo $comment['id']; ?>" class="cackle-comment-message">
                  <?php echo $comment['text']; ?>
                  </div>
              </div>
          </li><?php } 
    
     
     function cackle_display_comments(){
         global $cackle_api;
         $cackle_api = new CackleAPI();?>
        <div id="mc-container">

                <ul id="cackle-comments">
                 <?php $this->list_comments(); ?>
                </ul>

        </div>
        <script type="text/javascript">
        <?php $channel = $this->channel; ?>
        cackle_widget = window.cackle_widget || [];
        cackle_widget.push({widget: 'Comment', id: '<?php echo CackleAPI::cackle_get_param("cackle_apiId"); ?>', channel: '<?php echo($channel)?>'
            <?php if (CackleAPI::cackle_get_param('cackle_sso') == 1) : ?>, ssoAuth: '<?php print_r($this->cackle_auth()) ?>' <?php endif;?>   });
        document.getElementById('mc-container').innerHTML = '';
        (function() {
            var mc = document.createElement("script");
            mc.type = "text/javascript";
            mc.async = true;
            mc.src = ("https:" == document.location.protocol ? "https" : "http") + "://cackle.me/widget.js";
            var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(mc, s.nextSibling);
        })();
        </script>
     <?php if(CackleAPI::cackle_get_param('cackle_whitelabel') !=1 ){ ?>
         <a id="mc-link" href="http://cackle.me">Социальные комментарии <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a>



<?php }}
    function get_local_comments(){
        //getting all comments for special post_id from database.
        $cackle_api = new CackleAPI();
        $channel = $this->channel;
        $get_all_comments = $cackle_api->db_connect("select * from ".PREFIX.COMMENTS_TABLE." where post_id = $channel and approve = 1;");
        return $get_all_comments;
    }
    function list_comments(){
        $obj = $this->get_local_comments();
        if ($obj){
            foreach ($obj as $comment) {
                $this->cackle_comments($comment);
            }
        }
    }
}

if(isset($init_cackle)){

}
else{
    $a = new Cackle(true,$newsid);
}
?>
