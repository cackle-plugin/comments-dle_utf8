<?php
include_once ('engine/modules/cackle/cackle_api.php');

function echo_recent(){
    ob_start()?>

<div id="mc-last"></div>
<script type="text/javascript">
    cackle_widget = window.cackle_widget || [];
    cackle_widget.push({widget: 'CommentRecent', id:  '<?php echo CackleAPI::cackle_get_param("cackle_apiId"); ?>', size: 5,
        avatarSize: 32, textSize: 150, titleSize: 40});
    (function() {
        var mc = document.createElement('script');
        mc.type = 'text/javascript';
        mc.async = true;
        mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
    })();
</script>

<?php
    echo  ob_get_clean();
}
echo_recent();
?>