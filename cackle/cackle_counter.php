<?php
include_once ('engine/modules/cackle/cackle_api.php');

function echo_counter(){
$cackle_api = new CackleAPI();
ob_start()?>
<script type="text/javascript">
cackle_widget = window.cackle_widget || [];
cackle_widget.push({widget: 'CommentCount', id:  '<?php echo CackleAPI::cackle_get_param("cackle_apiId"); ?>',
    html: '<span title="Комментариев: {num}"><b>{num}</b></span>'
});
(function() {
    var mc = document.createElement('script');
    mc.type = 'text/javascript';
    mc.async = true;
    mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
})();
</script>

<?php
echo  ob_get_clean();
}
echo_counter();
?>