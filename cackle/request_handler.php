<?php

function get_comment_status($status){
    if ($status == "1") {
        $status = "approved";
    } elseif ($status == "0") {
        $status = "pending";
    } elseif ($status == "spam") {
        $status = "spam";
    } elseif ($status == "trash") {
        $status = "deleted";
    }
    return $status;
}
function get_msg($comment){
    if(CACKLE_ENCODING == "cp1251") {
        $comment_text = iconv('cp1251', 'utf-8', $comment->text);
        $comment_text=str_replace("<br />","\n",$comment_text);
        return $comment_text;
    }
    else{
        $comment_text = $comment->text;
        $comment_text=str_replace('<br />',"\n",$comment_text);
        return $comment_text;

    }
}
function render_json($array){
    ob_start();
    header('Content-type: text/javascript');
    $debug = ob_get_clean();
    echo json_encode($array);
    die();
}
if(!isset($_POST['data'])) return;
$post_req=json_decode(stripslashes($_POST['data']),true);
switch ($post_req['cackleApi']) {
    case 'export':
        //if (current_user_can('manage_options')) {
            global $cackle_api;
            $apix = new CackleAPI();
            $post_fields = (object) unserialize(POSTS_FIELDS);
            $timestamp = intval($post_req['timestamp']);
            $action = $post_req['action'];
            $offset = $post_req['offset'];
            $manual_export = CackleAPI::cackle_get_param('cackle_manual_export','');
            if($manual_export==''){
                $manual_export = new stdClass();
                $manual_export->status='export';
            }
            $post_id = intval($post_req['post_id']);

            switch ($action) {
                case 'export_start':
                    if($manual_export->status == 'stop'){
                        $result = 'fail';
                        ob_start();
                        $msg = '<div class="status cackle-export-fail error">' . cackle_i('export was stopped on processing post with id') . $post_id. '</div>';
                        $response = compact('result', 'timestamp', 'status', 'post_id', 'msg', 'eof', 'response', 'debug');
                        header('Content-type: text/javascript');
                        echo json_encode($response);
                        $manual_export->status = 'export'; //revert trigger for initial state
                        CackleAPI::cackle_set_param('cackle_manual_export',$manual_export);
                        die();

                    }
                    break;
            }


            ob_start();

            $dbh = $apix->conn();
            if (CACKLE_ENCODING=='cp1251'){

                $dbh->exec('SET NAMES cp1251');
            }
            else{
                $dbh->exec('SET NAMES utf8');
            }
            $sth = $dbh->prepare("SELECT *
                                FROM  ".PREFIX."_post where $post_fields->id > ? ORDER BY $post_fields->id ASC
                                LIMIT 1");
            $sth->execute(array($post_id));

            $post = $sth->fetchAll(PDO::FETCH_OBJ);
            $post = $post['0'];
            $post_id=$post->id;

            $dbh = $apix->conn();
            $sth = $dbh->prepare("SELECT MAX($post_fields->id) as max
                                FROM  ".PREFIX.POSTS_TABLE);
            $sth->execute();
            $max_post_id = $sth->fetchAll(PDO::FETCH_OBJ);
            $max_post_id = $max_post_id[0]->max;
            ////////////////////////
            $eof = (int)($post_id == $max_post_id);
            if ($eof) {
                $status = 'complete';
                $msg = 'Your comments have been sent to Cackle and queued for import!<br/>';
            } else {
                $status = 'partial';
                //require_once(dirname(__FILE__) . '/manage.php');
                $msg = print_r('Processed comments on post #%s&hellip;', $post_id);
                $manual_export->finish=false;
                CackleAPI::cackle_set_param('cackle_manual_export',$manual_export);
            }
            $result = 'fail';

            $response = null;
            if ($post) {
                $dbh = $apix->conn();
                if (CACKLE_ENCODING=='cp1251'){

                    $dbh->exec('SET NAMES cp1251');
                }
                else{
                    $dbh->exec('SET NAMES utf8');
                }
                $sth = $dbh->prepare("SELECT *
                            FROM  ".PREFIX."_comments where post_id = :post_id
                            AND user_agent NOT LIKE 'Cackle:%%' ORDER BY date ASC
                            limit :limit OFFSET :offset");

                $sth->bindValue(':post_id', $post_id);
                $sth->bindValue(':limit', (int) 100, PDO::PARAM_INT);
                $sth->bindValue(':offset', (int) $offset, PDO::PARAM_INT);
                $sth->execute();
                $comms = $sth->fetchAll(PDO::FETCH_OBJ);
                //$comments = $comments['0'];

                if(sizeof($comms)==0){
                    $response = "success";
                    $comments_pack_status = 'complete';
                    $comments_prepared = null;
                }
                else{
                    $comments=array();
                    foreach ($comms as $comment) {
                        $created=new DateTime($comment->date);
                        $dbh = $apix->conn();
                        $sth = $dbh->prepare("select * from ".PREFIX."_users where user_id = ?");
                        $sth->execute(array($comment->user_id));
                        $user_info = $sth->fetchAll(PDO::FETCH_OBJ);
                        $user_info = $user_info[0];
                        if($user_info->foto == ""){
                            $avatar_path=NULL;
                        }
                        else{
                            if($apix->startsWith($user_info->foto, 'http')){
                                $avatar_path=$user_info->foto;
                            }
                            else{
                                $avatar_path=AVATAR_PATH. $user_info->foto;
                            }
                        }


                        $comments[]=Array(
                            'id' => $comment->id,
                            'ip' => $comment->ip,
                            'status' => get_comment_status($comment->approve),
                            'msg'=> get_msg($comment),
                            'created' => $created->getTimestamp()*1000,
                            'user' => ($comment->user_id > 0) ? array(
                                'id' => $comment->user_id,
                                'name' => (CACKLE_ENCODING == "cp1251") ? iconv('cp1251', 'utf-8', $comment->autor):$comment->autor,
                                'avatar' => $avatar_path,
                                'email' => $comment->email
                            ) : null,
                            'parent' => $comment->parent,
                            'name' => ($comment->user_id == 0) ? ((CACKLE_ENCODING == "cp1251") ? iconv('cp1251', 'utf-8', $comment->autor):$comment->autor) : null,
                            'email' => ($comment->user_id == 0) ? $comment->email : null
                        );

                    }
                    $response = CackleAPI::import_wordpress_comments($comments,$post,$eof);
                    $response = json_decode($response,true);
                    $fail_response = $response;
                    $response = (isset($response['responseApi']['status']) && $response['responseApi']['status'] == "ok" ) ? "success" : "fail";
                    $comments_pack_status = 'partial';
                    $comments_prepared = sizeof($comments);
                }

                if (!($response == "success")) {
                    $result = 'fail';
                } else {
                    if ($eof) {
                        $manual_export->finish=true;
                        CackleAPI::cackle_set_param('cackle_manual_export',$manual_export);
                    }
                    $result = 'success';
                }
            }
            //AJAX response
            $debug = ob_get_clean();
            $export = 'export';
            $response = compact('result', 'timestamp', 'status', 'post_id', 'msg', 'eof', 'response', 'debug','export','fail_response','comments_pack_status','comments_prepared');
            header('Content-type: text/javascript');
            echo json_encode($response);
            //Update last post id exported only if it was exported
            if($result=='success'){
                $manual_export->last_post_id=$post_id;
                if($comments_pack_status == 'complete'){
                    $manual_export->last_offset=0;
                }
                else{
                    $manual_export->last_offset=$offset;
                }
                CackleAPI::cackle_set_param('cackle_manual_export',$manual_export,true);
            }

            die();
        //}
        break;
    case 'import_prepare':
        //Import prepare action is for clearing previous comments sync information and create
            //$timestamp = intval($post_req['timestamp']);
            $page = $post_req['offset']/100;
            $post_fields = (object) unserialize(POSTS_FIELDS);
            $manual_sync = CackleAPI::cackle_get_param('cackle_manual_sync', '');
            if ($manual_sync == '') {
                $manual_sync = new stdClass();
                $manual_sync->status = 'sync';
            }
            $post_id = intval($post_req['post_id']);
            if ($post_req['offset'] == 0) {
                CackleAPI::db_connect("delete from ".PREFIX."_comments where user_agent like 'Cackle:%%'");
                CackleAPI::db_connect("delete from ".PREFIX."_cackle_channel");
                CackleAPI::db_connect("delete from ".PREFIX."_cackle where param = 'cackle_monitor'");
                CackleAPI::db_connect("delete from ".PREFIX."_cackle where param = 'cackle_monitor_short'");
                CackleAPI::db_connect("delete from ".PREFIX."_cackle where param = 'cackle_modified_trigger'");
                CackleAPI::db_connect("delete from ".PREFIX."_cackle where param = 'prepare'");

                CackleAPI::db_connect("delete from ".PREFIX."_cackle where param = 'cackle_posts_update'");
                CackleAPI::db_connect("delete from ".PREFIX."_cackle where param = 'cackle_channel_modified_trigger'");


                //initialize monitor object if not exist
                if( !CackleAPI::cackle_get_param('cackle_monitor') ) {
                    $object = new stdClass();
                    $object->post_id = 0;
                    $object->time = 0;
                    $object->mode = "by_channel";
                    $object->status = "finish";
                    $object->counter = 0;
                    CackleAPI::cackle_set_param('cackle_monitor',$object);
                }

                if( !CackleAPI::cackle_get_param('cackle_monitor_short') ) {
                    $object = new stdClass();
                    $object->post_id = 0;
                    $object->time = 0;
                    $object->mode = "by_channel";
                    $object->status = "finish";
                    CackleAPI::cackle_set_param('cackle_monitor_short',$object);
                }

                //initialize modified triger object if not exist
                if(!CackleAPI::cackle_get_param('cackle_modified_trigger')){
                    $modified_triger = new stdClass();
                    CackleAPI::cackle_set_param('cackle_modified_trigger',$modified_triger);
                }


            }


            //Get all chans for site, check if posts with these chans exist in WP and then add to channels table
            $resp = json_decode(CackleAPI::get_all_channels(100,$page),true);
            $chans = isset($resp['chans'])?$resp['chans']:'undefined';

            $object = CackleAPI::cackle_get_param('cackle_monitor');
            $object->post_id = 0;
            $object->status = 'inprocess';
            $object->mode = 'all_comments';
            $object->time = time();
            CackleAPI::cackle_set_param('cackle_monitor',$object);

            if(isset($chans[0])){
                foreach ($chans as $chan) {
                    $post_id = intval($chan);
                    if($post_id==0) continue;

                    $apix = new CackleAPI();
                    $dbh = $apix->conn();
                    $sth = $dbh->prepare("SELECT *
                            FROM  ".PREFIX.POSTS_TABLE." where $post_fields->id = ? ORDER BY $post_fields->id ASC
                            LIMIT 1");

                    $sth->execute(array($chan['channel']));
                    $post = $sth->fetchAll(PDO::FETCH_OBJ);
                    if(!isset($post[0])) continue;
                    $post = $post['0'];
                    $post_field_id = $post_fields->id;
                    $post_id = $post->$post_field_id;


                    //check if post exist in channel's table, if not insert with 0 time marker(to invoke all_comments mode for sync)

                    $dbh = $apix->conn();
                    $sth = $dbh->prepare("SELECT *
                            FROM  ".PREFIX."_cackle_channel where $post_fields->id = ? ORDER BY $post_fields->id ASC
                            LIMIT 1");
                    $sth->execute(array($post_id));
                    $get_last_time = $sth->fetchAll(PDO::FETCH_OBJ);
                    //

                    if (count($get_last_time)==0) {

                        $cackle_api = new CackleAPI();
                        $conn = $cackle_api->conn();
                        if ($cackle_api->cackle_get_param("cackle_encoding") == 1){

                            $conn->exec('SET NAMES cp1251');
                        }
                        else{
                            $conn->exec('SET NAMES utf8');
                        }

                        $sql = "insert into " . PREFIX ."_cackle_channel (id,time) values (?,? ) ON DUPLICATE KEY UPDATE time = ?";
                        $q = $conn->prepare($sql);
                        $q->execute(array($post_id,0,0));
//                        $sql = "INSERT INTO {$wpdb->prefix}cackle_channel (id, time) VALUES (%s,%s) ON DUPLICATE KEY UPDATE time = %s";
//                        $sql = $wpdb->prepare($sql,$post_id,0,0);
//                        $wpdb->query($sql);
                    }
                }
                $arr['status'] = 'partial';
                $arr['channels_prepared'] = sizeof($chans);
            }
            else{
                $arr['status'] = 'complete';
            }
            render_json($arr);
            //TODO: create angular controller

        break;
    case 'import':
        $timestamp = intval($post_req['timestamp']);
        $post_fields = (object) unserialize(POSTS_FIELDS);
        $post_id = intval($post_req['post_id']);
        $manual_sync = CackleAPI::cackle_set_param('cackle_manual_sync','');
        if($manual_sync==''){
            $manual_sync = new stdClass();
            $manual_sync->status='sync';
        }
        if($post_id==0){
            CackleAPI::cackle_set_param("cackle_channel_modified_first",time()*1000);

        }

        $apix = new CackleAPI();
        $dbh = $apix->conn();
        $sth = $dbh->prepare("SELECT *
                            FROM  ".PREFIX."_cackle_channel where id > ? ORDER BY $post_fields->id ASC
                            LIMIT 1");
        $sth->execute(array($post_id));
        $post = $sth->fetchAll(PDO::FETCH_OBJ);
        $post = $post['0'];
        $post_field_id = $post_fields->id;
        $post_id = $post->$post_field_id;
        $dbh = null;


        $dbh = $apix->conn();
        $sth = $dbh->prepare("SELECT MAX($post_fields->id) as max
                            FROM  ".PREFIX.POSTS_TABLE);
        $sth->execute();
        $max_post_id = $sth->fetchAll(PDO::FETCH_OBJ);
        $max_post_id = $max_post_id[0]->max;
        $dbh = null;


        $eof = (int)($post_id == $max_post_id);
        if ($eof) {
            $status = 'complete';
            $msg = 'Your comments have been resynchronized!<br/>';
        } else {
            $status = 'partial';
            //require_once(dirname(__FILE__) . '/manage.php');
            $msg = print_r('Processed comments on post #%s&hellip;', $post_id);
            $manual_sync->finish=false;
            CackleAPI::cackle_set_param('cackle_manual_sync',$manual_sync);
        }
        $result = 'fail';
        ob_start();
        $response = null;
        if ($post) {
            $sync = new CackleSync();
            $response = $sync->init($post_id,'all_comments');
            $fail_response = $response;
            if (!($response == "success")) {
                $result = 'fail';
                $msg = '<p class="status cackle-export-fail">' . print_r('Sorry, something  happened with the export. Please <a href="#" id="cackle_export_retry">try again</a></p><p>If your API key has changed, you may need to reinstall Cackle (deactivate the plugin and then reactivate it). If you are still having issues, refer to the <a href="%s" onclick="window.open(this.href); return false">WordPress help page</a>.', 'http://cackle.me/help/') . '</p>';
                $response = $cackle_api->get_last_error();
            } else {
                if ($eof) {
                    //we need to switch monitor to by_channel
                    $object = new stdClass();
                    $object->mode = "by_channel";
                    $object->post_id = 0;
                    $object->status = 'finish';
                    $object->time = time();
                    CackleAPI::cackle_set_param('cackle_monitor',$object);
                    CackleAPI::cackle_set_param('cackle_monitor_short',$object);
                    $manual_sync->finish=true;
                    CackleAPI::cackle_set_param('cackle_manual_sync',$manual_sync);
                    $msg = print_r('Your comments have been synchronized with Cackle and queued for import!<br/>After exporting the comments you receive email notification', 'http://cackle.me/help/');
                }
                $result = 'success';
            }
        }
        //AJAX response
        $debug = ob_get_clean();
        $import='import';
        $response = compact('result', 'timestamp', 'status', 'post_id', 'msg', 'eof', 'response', 'debug','import','fail_response');
        header('Content-type: text/javascript');
        echo json_encode($response);
        if($result=='success') {
            $manual_sync->last_post_id = $post_id;
            CackleAPI::cackle_set_param('cackle_manual_sync', $manual_sync);
        }

        die();
        break;


}
switch ($post_req['cackleApi']) {
    case 'checkKeys':
        require_once(dirname(__FILE__) . '/cackle_activation.php');
        $resp = CackleActivation::check($post_req['value']);
        echo json_encode($resp);
        die();
}

?>