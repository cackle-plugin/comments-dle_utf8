<?php
ini_set('xdebug.max_nesting_level', 1100);
/*
 * Settings
 */

define('CACKLE_ENCODING', 'utf8');
define('AVATAR_PATH', $config['http_home_url'] . "uploads/fotos/");
/*Enter database settings*/
define('LOCAL_DB_HOST', DBHOST);
define('LOCAL_DB_NAME', DBNAME);
define('LOCAL_DB_USER', DBUSER);
define('LOCAL_DB_PASSWORD', DBPASS);

//Define prefix if you would like tables with prefix, ex. cackle_comments
//prefix is already defined
//define('PREFIX', 'cackle_');
define('COMMENTS_TABLE', '_comments');

//here you should enter id and date fields from you posts table
define('COMMENTS_FIELDS', serialize(
    array(
        'id' => 'id',
        'post_id' => 'post_id',
        'comment' => 'text',
        'date' => 'date',
        'autor' => 'autor',
        'email' => 'email',
        'avatar' => 'avatar',
        'ip' => 'ip',
        'approve' => 'approve',
        'user_agent' => 'user_agent',
        'user_id' => 'user_id'

    )
));
define('POSTS_TABLE', '_post');
define('POSTS_FIELDS', serialize(
    array(
        'id' => 'id',
        'post_date' => 'date'
    )
));


class CackleAPI{

    private static $cache = array();
    static function getConstants() {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
    function CackleAPI(){
        $this->last_error = null;
        $siteId = CackleAPI::cackle_get_param("cackle_apiId");
        $accountApiKey = CackleAPI::cackle_get_param("cackle_accountApiKey");
        $siteApiKey = CackleAPI::cackle_get_param("cackle_siteApiKey");
        $this->get_url = $get_url = "http://cackle.me/api/3.0/comment/list.json?id=$siteId&accountApiKey=$accountApiKey&siteApiKey=$siteApiKey";
    }
    function startsWith($haystack, $needle){
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
    static function db_connect($query){
        try {
            $hd="mysql:host=" . LOCAL_DB_HOST . ";dbname=" . LOCAL_DB_NAME;
            $DBH = new PDO($hd, LOCAL_DB_USER, LOCAL_DB_PASSWORD);
            if (CACKLE_ENCODING=='cp1251'){

                $DBH->exec('SET NAMES cp1251');
            }
			else{
			$DBH->exec('SET NAMES utf8');
			}
			
            $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $STH = $DBH->query($query);

            #  устанавливаем режим выборки
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $x=0;
            $row=array();
            while($res = $STH->fetch()) {
                $row[$x]=$res;
                $x++;
            }
            $DBH = null;
            return $row;
        }
        catch(PDOException $e) {
           // echo "invalid sql - $query - ";
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
        }
    }
    function conn(){
        try {
            $hd="mysql:host=" . LOCAL_DB_HOST . ";dbname=" . LOCAL_DB_NAME;
            $DBH = new PDO($hd, LOCAL_DB_USER, LOCAL_DB_PASSWORD,
            array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_PERSISTENT => false,
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci"
            )
            );
            
            
            
            return $DBH;
        }
        catch(PDOException $e) {
            return null;
            //echo "invalid sql - $query - ";
            //file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
        }
    }
    static function get_all_channels($size,$page,$modified=0){
        $siteId = CackleAPI::cackle_get_param("cackle_apiId");
        $accountApiKey = CackleAPI::cackle_get_param("cackle_accountApiKey");
        $siteApiKey = CackleAPI::cackle_get_param("cackle_siteApiKey");
        $url_base = "http://cackle.me/api/3.0/comment/chan/list.json?id=$siteId&siteApiKey=$siteApiKey&accountApiKey=$accountApiKey&size=$size&page=$page";
        if($modified){
            $url = $url_base . "&gtModify=$modified";
        }
        else{
            $url = $url_base;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");
        //curl_setopt($ch,CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-type: application/x-www-form-urlencoded; charset=utf-8',
            )
        );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;


    }
    static function import_wordpress_comments($comments, $post, $eof = true){
        $url = "http://" . $_SERVER["SERVER_NAME"] . "/index.php?newsid=" . $post->id;
        if (CACKLE_ENCODING == "cp1251"){
            $title = iconv('cp1251', 'utf-8', $post->title);
        }
        $data = array(
            'chan' => $post->id,
            'url' => urlencode($url),
            'title' => $title,
            'comments' => $comments);
        $postfields = json_encode($data);
        $params = array(
            'id' => CackleAPI::cackle_get_param("cackle_apiId"),
            'accountApiKey' => CackleAPI::cackle_get_param("cackle_accountApiKey"),
            'siteApiKey' => CackleAPI::cackle_get_param("cackle_siteApiKey")
        );
        $curl = curl_init('http://cackle.me/api/3.0/comment/post.json?'.http_build_query($params));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($postfields))
        );
        $response = curl_exec($curl);
        if(curl_errno($curl)){
            $result = 'Ошибка curl: ' . curl_error($curl);
            $response = compact('result');
            header('Content-type: text/javascript');
            //echo $result;
            $arr = array();
            $arr['responseApi']['status']= 'fail';
            $arr['responseApi']['error']='Cackle not responded';
            return json_encode($arr);
        }


        curl_close($curl);

        /*if ($response['body'] == 'fail') {
            $this->api->last_error = $response['body'];
            return -1;
        }
        $data = $response['body'];
        if (!$data || $data == 'fail') {
            return -1;
        }*/

        return $response;
    }
    function get_comments($criteria, $cackle_last, $post_id, $cackle_page = 0) {
        //$time_start = microtime(true);
        if ($criteria =='last_comment'){
            $host = $this->get_url . "&commentId=" . $cackle_last  . "&size=100&chan=" . $post_id;
        }
        if ($criteria =='last_modified'){
            $host = $this->get_url . "&modified=" . $cackle_last . "&page=" . $cackle_page . "&size=100&chan=" . $post_id;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");
        //curl_setopt($ch,CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-type: application/x-www-form-urlencoded; charset=utf-8',
            )
        );

        $result = curl_exec($ch);

        curl_close($ch);

        //$trace=debug_backtrace();
        //$function = $trace[0]["function"];
        //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
        //file_put_contents('execution_time.txt', $mess, FILE_APPEND);

        return $result;
    }

    function get_last_comment_by_channel($channel,$default){
        if ( $dbh = $this->conn()){
            $sth = $dbh->prepare("SELECT last_comment FROM ".PREFIX."_cackle_channel WHERE id = ?");
            $sth->execute(array($channel));
            $result = $sth->fetchAll(PDO::FETCH_OBJ);
        }
        else{
            return null;
        }

        if(sizeof($result)>0){
            $result = $result[0]->last_comment;
            if(is_null($result)){
                return $default;
            }
            else{
                return $result;
            }
        }
    }
    function set_last_comment_by_channel($channel,$last_comment){
        //$time_start = microtime(true);

        if ( $dbh = $this->conn()) {
            $b = $dbh->prepare("UPDATE `".PREFIX."_cackle_channel` SET last_comment=:last_comment WHERE id =:channel");
            $b->bindParam(":last_comment", $last_comment);
            $b->bindParam(":channel", $channel);
            $b->execute();
        }

        //Profiller
        //$trace=debug_backtrace();
        //$function = $trace[0]["function"];
        //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
        //file_put_contents('execution_time.txt', $mess, FILE_APPEND);

    }



    function set_monitor_status($status){

    }
    function get_last_modified_by_channel($channel,$default){
        //$time_start = microtime(true);

        if ( $dbh = $this->conn()){
            $sth = $dbh->prepare("SELECT modified FROM ".PREFIX."_cackle_channel WHERE id = ?");
            $sth->execute(array($channel));
            $result = $sth->fetchAll(PDO::FETCH_OBJ);
        }
        else{
            return null;
        }

        if(sizeof($result)>0){
            $result = $result[0]->modified;
            if(is_null($result)){

                //$trace=debug_backtrace();
                //$function = $trace[0]["function"];
                //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
                //file_put_contents('execution_time.txt', $mess, FILE_APPEND);

                return $default;
            }
            else{
                //$trace=debug_backtrace();
                //$function = $trace[0]["function"];
                //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
                //file_put_contents('execution_time.txt', $mess, FILE_APPEND);

                return $result;
            }
        }
        $res= $result;
    }
    function set_last_modified_by_channel($channel,$last_modified){
        //$time_start = microtime(true);

        if ( $dbh = $this->conn()) {
            $b = $dbh->prepare("UPDATE `".PREFIX."_cackle_channel` SET modified=:modified WHERE id =:channel");
            $b->bindParam(":modified", $last_modified);
            $b->bindParam(":channel", $channel);
            $b->execute();
        }

        //$trace=debug_backtrace();
        //$function = $trace[0]["function"];
        //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
        //file_put_contents('execution_time.txt', $mess, FILE_APPEND);

    }



    static function db_table_exist($table){
        $hd="mysql:host=" . LOCAL_DB_HOST . ";dbname=" . LOCAL_DB_NAME;
        $DBH = new PDO($hd, LOCAL_DB_USER, LOCAL_DB_PASSWORD);
        $tableExists = (gettype($DBH->exec("SELECT count(*) FROM $table")) == "integer")?true:false;
        return $tableExists;
    }
    static function db_column_exist($table,$column){
        if (self::db_table_exist($table)){
            $hd="mysql:host=" . LOCAL_DB_HOST . ";dbname=" . LOCAL_DB_NAME;
            $DBH = new PDO($hd, LOCAL_DB_USER, LOCAL_DB_PASSWORD);
            $quer= "SHOW COLUMNS FROM $table LIKE '$column'";
            $column_exist = $DBH->query($quer)->fetch();
            $column_exist = $column_exist['Field'];
            //$column_exist = (gettype($DBH->query("SHOW COLUMNS FROM $table LIKE '$column''")) == "integer")?true:false;
            return $column_exist;
            //return $quer;
        }
        else {
            return false;
        }
    }
    static function cackle_set_param_db($param, $value){
        $hd="mysql:host=" . LOCAL_DB_HOST . ";dbname=" . LOCAL_DB_NAME;
        $DBH = new PDO($hd, LOCAL_DB_USER, LOCAL_DB_PASSWORD);

        if (self::db_table_exist("".PREFIX."_cackle")){
            $DBH->query("delete from ".PREFIX."_cackle where param = '$param'");
            $DBH->query("insert into ".PREFIX."_cackle (param, value) values ('$param','$value')");
        }
        else{
            self::db_connect("CREATE TABLE ".PREFIX."_cackle (param text NOT NULL DEFAULT '',value text NOT NULL DEFAULT '')");
            //self::db_table_exist();
        }
    }

    static function cackle_get_param_db($param,$default=0){
        $hd="mysql:host=" . LOCAL_DB_HOST . ";dbname=" . LOCAL_DB_NAME;
        $DBH = new PDO($hd, LOCAL_DB_USER, LOCAL_DB_PASSWORD);

        if (self::db_table_exist("".PREFIX."_cackle")){
            $ex = $DBH->query("select value from ".PREFIX."_cackle where param = '$param'")->fetch();
            $res = $ex['value'];
            if ($res == null){
                $res = 0;
            }
            return $res;
        }
        else{
            self::cackle_db_prepare();
        }
    }

    static function cackle_get_param($param,$default=0){

        if(!isset(self::$cache[$param])) {
            //echo 'once';
            self::$cache[$param] = self::cackle_get_param_db($param,$default);
        }
        //check param is serialized object
        $data = @unserialize(self::$cache[$param]);

        if ($param === 'b:0;' || $data !== false) {
            self::$cache[$param] = $data;
        } else {

        }
        return isset(self::$cache[$param]) ? self::$cache[$param] : $default;
    }
    static function cackle_set_param($param,$value){
        //to DB function
        unset(CackleAPI::$cache[$param]);
        if(is_object($value)){
            $value = serialize($value);
        }
        self::cackle_set_param_db($param, $value);
    }

    function cackle_db_prepare(){

        if (self::db_table_exist("".PREFIX.COMMENTS_TABLE."")){
            self::db_connect("ALTER TABLE ".PREFIX.COMMENTS_TABLE." ADD user_agent VARCHAR(64) default ''");
           // $this->db_connect("ALTER TABLE ".PREFIX.COMMENTS_TABLE." MODIFY 'user_agent' varchar(64) NOT NULL default ''");
        }
        else {
            $create_comment_sql = "
        CREATE TABLE ".PREFIX.COMMENTS_TABLE." (
	id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	post_id TEXT NOT NULL,
	comment TEXT NOT NULL,
	date DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	autor VARCHAR(40) NOT NULL DEFAULT '',
	email VARCHAR(40) NULL DEFAULT '',
	avatar VARCHAR(50) NULL DEFAULT NULL,
	ip VARCHAR(16) NOT NULL DEFAULT '',
	is_register TINYINT(1) NOT NULL DEFAULT '0',
	approve TINYINT(1) NOT NULL DEFAULT '1',
	user_agent VARCHAR(64) NOT NULL DEFAULT '',
	PRIMARY KEY (id)
)


        ";
        self::db_connect($create_comment_sql);
        }

        if (self::db_table_exist("".PREFIX."_cackle_channel")){
           // $this->db_connect("ALTER TABLE ".PREFIX.COMMENTS_TABLE." ADD user_agent VARCHAR(64) NOT NULL default ''");
            // $this->db_connect("ALTER TABLE ".PREFIX.COMMENTS_TABLE." MODIFY 'user_agent' varchar(64) NOT NULL default ''");
        }
        else {
            $create_comment_sql = "
        CREATE TABLE ".PREFIX."_cackle_channel (
	            id varchar(150) NOT NULL DEFAULT '',
	            time bigint(11) NOT NULL,
	            modified varchar(25) DEFAULT NULL,
	            last_comment varchar(250) DEFAULT NULL,
                PRIMARY KEY (id),
	            UNIQUE KEY id (id)
)


        ";
        self::db_connect($create_comment_sql);
        }

        //CackleAPI::cackle_set_param('prepare',1);

    }

    function get_last_error() {
        if (empty($this->last_error)) return;
        if (!is_string($this->last_error)) {
            return var_export($this->last_error);
        }
        return $this->last_error;
    }
    function curl($url) {
        $ch = curl_init();
        $php_version = phpversion();
        $useragent = "Drupal";
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("referer" =>  "localhost"));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function key_validate($id, $site, $account)
    {
        $key_url = "http://cackle.me/api/2.0/site/info.json?id=$id&accountApiKey=$account&siteApiKey=$site";
        return  self::curl($key_url);

    }

}